﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using NotificationShared.proto;

namespace GrpcClient
{
    class Program
    {
        //for local use: localhost:8080, for Docker Compose use: server:8080
        private const string Target = "grpcserver:8080";

        static async Task Main(string[] args)
        {
            //gRPC channel based on Grpc.Core
            var hostadressDocker = Environment.GetEnvironmentVariable("SERVER_STREAM_ENDPOINT");

            Console.WriteLine($"Docker adress is: {hostadressDocker}");
            if (string.IsNullOrEmpty(hostadressDocker))
            {
                hostadressDocker = Target;
            }
            Channel channel = new Channel(Target, ChannelCredentials.Insecure);

            await channel.ConnectAsync().ContinueWith((task) =>
            {
                if (task.Status == TaskStatus.RanToCompletion)
                {
                    Console.WriteLine("The client connected successfully");
                }
            });

            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
            var client = new NotificationService.NotificationServiceClient(channel);

            // Composed object send with Unary message with loop
            foreach (var i in Enumerable.Range(1, 2))
            {
                Console.WriteLine("Starting Unary run \n");
                UnaryComposedMessages(client);
            }
            
            // Composed object with Server stream with loop on server
            foreach (var i in Enumerable.Range(1, 2))
            {
                Console.WriteLine("Starting Server stream run \n");
                await ServerStreamComposedMessages(client);
            }

            Console.WriteLine("Test ended, restart Client container to run tests again \n\n");
            
            await channel.ShutdownAsync();
        }

        private const string ImplementationVariant = "gRPC with Protobuf";
        private const string OutputFile = "UnaryProtobufsmall.txt";
        public static void UnaryComposedMessages(NotificationService.NotificationServiceClient client)
        {
            Console.WriteLine($"Testing {ImplementationVariant} \n");
            Console.WriteLine("Waiting 3000 milliseconds to start client server... Go!!");
            Task.Delay(3000);
            var sw = new Stopwatch();
            var composedClassResponse = new ComposedClassResponse();
            var composedClassRequests = new List<ComposedClassRequest>();
            Console.WriteLine($"Results document path is: {Path.Combine(Environment.CurrentDirectory, $"{OutputFile}")}\n");
            var composedClassList = GetComposedClass(100);
            foreach (var composedClass in composedClassList)
            {
                composedClassRequests.Add(new ComposedClassRequest() { ComposedClass = composedClass });
            }
            Console.WriteLine($"Starting loop from {ImplementationVariant} with Unary service");
            sw.Start();
            foreach (var index in composedClassRequests)
            {
                composedClassResponse = client.UnaryComposedClassMessage(index);
            }
            sw.Stop();
            Console.WriteLine($"took {sw.Elapsed.TotalMilliseconds} milliseconds to complete");
            var result = $"Sending Composed objects: \n " +
                         $"{composedClassResponse.ComposedClass.Id} times with: \n " +
                         $"{composedClassResponse.ComposedClass.JustAList.Count} list items \n " +
                         $"{composedClassResponse.ComposedClass.JustAList[0].Notifications.Count} Notifications \n " +
                         $"{composedClassResponse.ComposedClass.KeyValuePairs.Count} dictionary items \n\n" +
                         $"took {sw.Elapsed.TotalMilliseconds} milliseconds to complete";
            Console.WriteLine(result);

            Task.Run(() =>
            {
                using var streamWriter = new StreamWriter(Path.Combine(Environment.CurrentDirectory, $"{OutputFile}"), append: true);

                streamWriter.Write($"{sw.Elapsed.TotalMilliseconds} \n");
            });
        }

        public static async Task ServerStreamComposedMessages(NotificationService.NotificationServiceClient client)
        {
            Console.WriteLine("Testing gRPC with Protobuf \n");
            await Task.Delay(3000);
            Console.WriteLine("Waited 3000 milliseconds to start client server... Go!!");
            Console.WriteLine("\n\nFor results see server output");
            List<ComposedClassResponse> list = new List<ComposedClassResponse>();
            var composedStreamResponse = client.ServerStreamComposedClass(new ComposedClassRequest() { ComposedClass = GetComposedClass(1)[0] });
            while (await composedStreamResponse.ResponseStream.MoveNext())
            {
                list.Add(composedStreamResponse.ResponseStream.Current);
            }
        }

        public static List<ComposedClass> GetComposedClass(int times)
        {
            var notifications = new List<Notification>();
            var notificationIds = new List<NotificationId>();
            var justADictionary = new Dictionary<int, string>();
            var composedClassList = new List<ComposedClass>();

            foreach (var id in Enumerable.Range(1, times))
            {
                var composedClass = new ComposedClass()
                {
                    Id = id,
                    Bar = 10 + id,
                    Foe = 20 + id,
                };
                foreach (var item in Enumerable.Range(1, 5))
                {
                    foreach (var index in Enumerable.Range(1, 5))
                    {
                        notifications.Add(new Notification()
                        {
                            Id = index,
                            Message = $"Notification item {index}"
                        });
                    }

                    var notificationId = new NotificationId()
                    {
                        Id = item,
                        Bar = 100 + item,
                        Foe = 200 + item,

                    };

                    notificationId.Notifications.Add(notifications);
                    notifications = new List<Notification>();
                    notificationIds.Add(notificationId);
                    justADictionary.Add(item, $"dictionary item: {item}");
                }
                composedClass.JustAList.Add(notificationIds);
                notificationIds = new List<NotificationId>();
                composedClass.KeyValuePairs.Add(justADictionary);
                justADictionary = new Dictionary<int, string>();
                composedClassList.Add(composedClass);
            }

            return composedClassList;
        }

        public static List<NotificationId> GetNotificationIds()
        {
            var notifications = new List<Notification>();
            var notificationIds = new List<NotificationId>();

            foreach (var item in Enumerable.Range(1, 25))
            {
                foreach (var index in Enumerable.Range(1, 25))
                {
                    notifications.Add( new Notification()
                    {
                        Id = index,
                        Message = $"Notification item {index}"
                    });
                }

                var notificationId = new NotificationId()
                {
                    Id = item,
                    Bar = 100 + item,
                    Foe = 200 + item,
                    
                };

                notificationId.Notifications.Add(notifications);
                notifications = new List<Notification>();
                notificationIds.Add(notificationId);
            }

            return notificationIds;
        }
    }
}