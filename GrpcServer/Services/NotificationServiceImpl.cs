﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using NotificationShared.proto;

namespace GrpcServer.Services
{
    class NotificationServiceImpl : NotificationService.NotificationServiceBase
    {
        public override Task<NotificationResponse> UnaryNotificationMessage(NotificationRequest request, ServerCallContext context)
        {
            return Task.FromResult(new NotificationResponse()
            {
                Notification = new Notification()
                {
                    Id = request.Notification.Id, Message = request.Notification.Message
                }
            });
        }

        public override Task<NotificationIdResponse> UnaryNotifiIdMessage(NotificationIdRequest request, ServerCallContext context)
        {
            return Task.FromResult(new NotificationIdResponse()
            {
                NotificationId = new NotificationId()
                {
                    Id = request.NotificationId.Id,
                    Bar = request.NotificationId.Bar,
                    Foe = request.NotificationId.Foe,
                    Notifications = { request.NotificationId.Notifications}
                }
            });
        }

        public override Task<ComposedClassResponse> UnaryComposedClassMessage(ComposedClassRequest request, ServerCallContext context)
        {
            return Task.FromResult(new ComposedClassResponse()
            {
                ComposedClass = new ComposedClass()
                {
                    Id = request.ComposedClass.Id,
                    Bar = request.ComposedClass.Bar,
                    Foe = request.ComposedClass.Foe,
                    KeyValuePairs = { request.ComposedClass.KeyValuePairs},
                    JustAList = { request.ComposedClass.JustAList}
                }
            });
        }

        private const string ImplementationVariant = "gRPC with Protobuf";
        private const string OutputFile = "ServerStreamProtobufsmall.txt";
        public override async Task ServerStreamComposedClass(ComposedClassRequest request, IServerStreamWriter<ComposedClassResponse> responseStream, ServerCallContext context)
        {
            _ = new ComposedClass()
            {
                Id = request.ComposedClass.Id,
                Bar = request.ComposedClass.Bar,
                Foe = request.ComposedClass.Foe,
                KeyValuePairs =
                {
                    request.ComposedClass.KeyValuePairs
                },
                JustAList =
                {
                    request.ComposedClass.JustAList
                }
            };

            Console.WriteLine($"Testing {ImplementationVariant} \n");
            var sw = new Stopwatch();
            var composedClass = new ComposedClass();
            var composedClassResponses = new List<ComposedClassResponse>();
            Console.WriteLine($"Results document path is: {Path.Combine(Environment.CurrentDirectory, $"{OutputFile}")}\n");
            var composedClassList = GetComposedClass(100);
            foreach (var index in composedClassList)
            {
                composedClassResponses.Add(new ComposedClassResponse() { ComposedClass = index });
            }
            Console.WriteLine($"Starting loop from {ImplementationVariant} with Server stream service\n");
            sw.Start();
            foreach (var index in composedClassResponses)
            {
                await responseStream.WriteAsync(index);
                composedClass = index.ComposedClass;
            }
            sw.Stop();
            var result = $"Sending Composed objects: \n " +
                         $"{composedClass.Id} times with: \n " +
                         $"{composedClass.JustAList.Count} list items \n " +
                         $"{composedClass.JustAList[0].Notifications.Count} Notifications \n " +
                         $"{composedClass.KeyValuePairs.Count} dictionary items \n\n" +
                         $"took {sw.Elapsed.TotalMilliseconds} milliseconds to complete";
            Console.WriteLine(result);

            await Task.Run(() =>
            {
                using var streamWriter = new StreamWriter(Path.Combine(Environment.CurrentDirectory, $"{OutputFile}"), append: true);
                streamWriter.Write($"{sw.Elapsed.TotalMilliseconds} \n");
            });
        }


        public static List<ComposedClass> GetComposedClass(int times)
        {
            var notifications = new List<Notification>();
            var notificationIds = new List<NotificationId>();
            var justADictionary = new Dictionary<int, string>();
            var composedClassList = new List<ComposedClass>();

            foreach (var id in Enumerable.Range(1, times))
            {
                var composedClass = new ComposedClass()
                {
                    Id = id,
                    Bar = 10 + id,
                    Foe = 20 + id,
                };
                foreach (var item in Enumerable.Range(1, 5))
                {
                    foreach (var index in Enumerable.Range(1, 5))
                    {
                        notifications.Add(new Notification()
                        {
                            Id = index,
                            Message = $"Notification item {index}"
                        });
                    }

                    var notificationId = new NotificationId()
                    {
                        Id = item,
                        Bar = 100 + item,
                        Foe = 200 + item,

                    };

                    notificationId.Notifications.Add(notifications);
                    notifications = new List<Notification>();
                    notificationIds.Add(notificationId);
                    justADictionary.Add(item, $"dictionary item: {item}");
                }
                composedClass.JustAList.Add(notificationIds);
                notificationIds = new List<NotificationId>();
                composedClass.KeyValuePairs.Add(justADictionary);
                justADictionary = new Dictionary<int, string>();
                composedClassList.Add(composedClass);
            }

            return composedClassList;
        }
    }
}
