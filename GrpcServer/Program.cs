﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using NotificationShared.proto;
using GrpcServer.Services;

namespace GrpcServer
{
    class Program
    {
        private const int Port = 8080;
        private static EventWaitHandle QuiteEvent = new AutoResetEvent(false);
        static async Task Main(string[] args)
        {
            try
            {
                await RunAsync();
            }
            catch (IOException e)
            {
                Console.WriteLine($"Server failed to start because: {e}");
                throw;
            }
        }

        private static async Task RunAsync()
        {
            Server server = new Server()
            {
                Services = {
                    NotificationService.BindService(new NotificationServiceImpl())
                },
                Ports = { new ServerPort("0.0.0.0", Port, ServerCredentials.Insecure) }
            };

            server.Start();
            Console.WriteLine($"Server is listing on the port: {Port}");
            QuiteEvent.WaitOne();

            await server.ShutdownAsync();
        }
    }
}
